# Pengenalan Spring Boot

## Daftar Isi
- [Tujuan](#tujuan)
- [Yang Dipelajari](#yang-dipelajari)
- [Ukuran Pencapaian](#ukuran-pencapaian)
- [Mengenal Spring Boot](#mengenal-spring-boot)
- [Bacaan Lanjut](#bacaan-lanjut)

## Tujuan
1. Mengenal framework Spring Boot dan mengetahui kegunaannya dalam pembangunan aplikasi web

## Yang Dipelajari
1. Apa itu Spring Boot

## Ukuran Pencapaian
1. Mengerti Spring Boot secara umum dan memiliki gambaran bagaimana jika tidak menggunakan Spring Boot

## Mengenal Spring Boot
Spring Boot adalah framework sumber terbuka Java yang dikembangkan oleh Pivotal Team, yang merupakan bagian dari VMware. Spring Boot dirancang untuk mempermudah pengembangan aplikasi Java dengan menyediakan pendekatan yang cepat, dengan cara menyediakan fasilitas-fasilitas yang umumnya banyak dibutuhkan dalam aplikasi enterprise.

Spring Boot menyodorkan pola yang "opiniated" pada aplikasi yang dibangun. Opinionated di sini berarti rancangan dasar aplikasi mengikuti pola yang baik menurut pendapat Pivotal Team (sesuai opini umum para pengembang aplikasi enterprise). Jadi dengan menggunakan Spring Boot, pengembang dapat memulai dengan cepat tanpa perlu banyak konfigurasi manual, karena banyak konvensi dan konfigurasi default yang sudah ada.

Spring Boot menyediakan berbagai fitur yang sangat berguna, termasuk:
- Konfigurasi otomatis: Spring Boot secara otomatis mengkonfigurasi banyak aspek aplikasi berdasarkan dependensi yang disertakan.
- Penyediaan embedded server: Spring Boot dapat menyertakan server aplikasi tertanam (seperti Tomcat, Jetty, atau Undertow) sehingga Anda dapat menjalankan aplikasi Anda tanpa harus mengonfigurasi server secara terpisah.
- Manajemen dependensi: Spring Boot menyederhanakan manajemen dependensi dengan menyediakan starter dependencies yang mengelola dependensi-transitif dan konfigurasi yang diperlukan.
- Banyak fitur lainnya seperti dukungan untuk pengujian otomatis, pemantauan, dan manajemen konfigurasi.

Secara keseluruhan, Spring Boot telah menjadi pilihan populer bagi pengembang Java untuk membangun aplikasi web dan microservice, karena kemudahan penggunaannya dan dukungan yang kuat dari komunitas.

Spring Boot adalah bagian dari Spring yang telah disederhanakan. Namun demikian, kadang kita jumpai hal-hal yang memerlukan kita untuk kembali ke Spring untuk dapat pemahaman yang baik.

## Bacaan Lanjut
Silakan baca lebih lanjut untuk mengenal Spring Boot dan Spring secara umum di:
- [Situs resmi Spring](https://spring.io)
- [Why Spring?](https://spring.io/why-spring)
- [Beragam Guides disertai contoh](https://spring.io/guides)
